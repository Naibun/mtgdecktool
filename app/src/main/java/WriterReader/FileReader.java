package WriterReader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import Deck.Deck;

public class FileReader {

    private static final String EMPTY_STRING = "";

    public Deck load(String aFileName){
        String file = buildFileName(aFileName);
        Deck loadedDeck = null;

        try{
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
            loadedDeck = (Deck) in.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return loadedDeck;
    }


    private String buildFileName(String aFileName) {
        StringBuilder fileName = new StringBuilder();

        if (!EMPTY_STRING.equals(aFileName) && null != aFileName) {
            fileName.append(aFileName);
            fileName.append(".txt");
        } else {
            throw new UnsupportedOperationException("Kein Dateiname gefunden");
        }

        return fileName.toString();
    }
}
