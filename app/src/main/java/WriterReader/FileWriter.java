package WriterReader;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import Deck.Deck;

public class FileWriter {

    private static final String EMPTY_STRING = "";
    private static final String NEW_DECK = "newDeck";

    public void save (Deck aDeck){

        String fileName = createFileName(aDeck.getName());

        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName));
            out.writeObject(aDeck);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String createFileName(String aDeckName) {
        StringBuilder fileName = new StringBuilder();

        if (!EMPTY_STRING.equals(aDeckName) && null != aDeckName) {
            fileName.append(aDeckName);
        } else {
            fileName.append(NEW_DECK);
        }

        fileName.append(".txt");

        return fileName.toString();
    }
}
