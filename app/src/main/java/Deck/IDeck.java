package Deck;

import io.magicthegathering.javasdk.resource.Card;

interface IDeck {

    public void setDeckSize();

    public void setCardLimit();

    public void setFormat();

    public void addCardToList(Card card);

}
