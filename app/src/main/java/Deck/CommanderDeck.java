package Deck;

import Card.CardConstants;

import io.magicthegathering.javasdk.resource.Card;

public class CommanderDeck extends Deck implements IDeck {

    private Card commander;

    public void setCommander(Card commander) {
        this.commander = commander;
        this.colorIdentity = commander.getColorIdentity();
        addCardToList(commander);
    }

    final Object getCommander() {
        return this.commander;
    }

    @Override
    public void setDeckSize() {
        this.deckSize = 100;
    }

    @Override
    public int getDeckSize() {
        return 100;
    }

    @Override
    public void setCardLimit() {
        this.cardLimit = 1;
    }

    @Override
    public void setFormat() {
        this.format = DeckConstants.COMMANDER;
    }

    @Override
    public void addCardToList(Card aCard) {
        if (getActualDeckSize() < this.deckSize && isLegalToAdd((aCard))) {
            this.deckList.add(aCard);
        }
    }

    @Override
    public void removeCardFromList(String aCardName) {
        for(int i = 0; i < this.getActualDeckSize(); i++) {
            Card card = this.deckList.get(i);
            if (aCardName.equals(card.getName())) {
                this.deckList.remove(i);
                i = this.getActualDeckSize();
            }
        }
    }
    /**
     * Hilfsmethode, die prüft, ob die zusätzlichen Regeln für den Deckbau eingehalten sind
     * (max. eine Kopie je Karte (außer Standardland)
     * @param aCard Die Karte, die potentiell hinzugefügt werden soll
     * @return true, wenn es zulässig ist. die Karte hinzuzufügen; ansonsten false
     */
    private boolean isLegalToAdd(Card aCard) {
        boolean isLegal = true;
        if (!isNotYetIncluded(aCard) || !matchesColorIdentity(aCard)) {
            isLegal = false;
        }
        return isLegal;
    }

    private boolean isNotYetIncluded(Card aCard) {
        boolean notIncluded = true;
        String type = aCard.getType();

        if (!CardConstants.BASIC_LAND.equals(type) && this.deckList.contains(aCard)) {
            notIncluded = false;
        }
        return notIncluded;

    }
    private boolean matchesColorIdentity(Card aCard) {
        boolean matchesIdentity = true;
        String partIdentityCard;
        String partIdentityDeck;

        String[] cardColorIdentity = aCard.getColorIdentity();
        for (int i = 0; i < cardColorIdentity.length; i++) {
            partIdentityCard = cardColorIdentity[i];
            for (int j = 0; j < this.colorIdentity.length; j++) {
                partIdentityDeck = this.colorIdentity[j];
                if (partIdentityCard.contentEquals(partIdentityDeck)) {
                    j = this.colorIdentity.length;
                    matchesIdentity = true;
                } else {
                    matchesIdentity = false;
                }
            }
        }
        return matchesIdentity;
    }

}
