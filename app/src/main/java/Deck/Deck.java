package Deck;


import java.io.Serializable;
import java.util.List;

import io.magicthegathering.javasdk.resource.Card;

public abstract class Deck implements IDeck, Serializable {
    List<Card> deckList;
    String[] colorIdentity;
    String format;
    String name;
    int deckSize;
    int cardLimit;

    public void Deck() {
    }

    public abstract void setDeckSize();

    public abstract void setCardLimit();

    public abstract void setFormat();


    public void setColorIdentity(String[] colorIdentity){
        this.colorIdentity = colorIdentity;
    }
    public final String[] getColorIdentity(){
        return this.colorIdentity;
    }

    public final String getFormat(){
        return this.format;
    }

    public void setName(String name){
        this.name = name;
    }
    final public String getName(){
        return this.name;
    }

    public void setDeckList(List<Card> decklist){
        this.deckList = decklist;
    }
    final public List<Card> getDeckList(){
        return this.deckList;
    }

    public int getDeckSize(){
        return this.deckSize;
    }


    public int getActualDeckSize(){
        return this.deckList.size();

    }

    public abstract void addCardToList(Card card);

    public abstract void removeCardFromList(String aCardName);
}
