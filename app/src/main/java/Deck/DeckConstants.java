package Deck;

public class DeckConstants {

    // Decktypes
    public static final String COMMANDER = "commander";
    public static final String STANDARD = "standard";
    public static final String BRAWL = "brawl";

}
