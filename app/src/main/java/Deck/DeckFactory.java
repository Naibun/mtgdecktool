package Deck;

public class DeckFactory {
    public Deck createNewDeck (String deckFormat){
        if(deckFormat.equals(DeckConstants.COMMANDER)){
            return new CommanderDeck();
        }
        return null;
    }
}
