package Deck.DeckList;

import java.util.ArrayList;
import java.util.List;

import io.magicthegathering.javasdk.resource.Card;

public class DeckList {
    private static int maxSize = 0;

    //Singleton: es darf nur eine Liste geben!!
    private static List<Card> theOnlyList;

    private static final String LIMITERREICHT = "Die maximale Deckgröße ist erreicht; " +
            "es können keine Karten mehr hinzugefügt werden.";

    public static List<Card> getDeckListInstance(int aMaxSize) {
        if (null == theOnlyList) {
            theOnlyList = new ArrayList<Card>(aMaxSize);
            maxSize = aMaxSize;
        }
        return theOnlyList;
    }

    public static void addCardToDeckList(Card aCard) {
        if (theOnlyList.size() < maxSize) {
            theOnlyList.add(aCard);
        }
        else {
            System.out.println(LIMITERREICHT);
        }
    }


}
