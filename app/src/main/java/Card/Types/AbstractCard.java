package Card.Types;

public abstract class AbstractCard {
    private String name;
    private String type;
    Boolean isLegendary;

    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }

    public String getType(){
        return this.type;
    }
    public abstract void setType();
}
