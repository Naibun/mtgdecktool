package Card.Helper;

import java.util.ArrayList;
import java.util.List;

import io.magicthegathering.javasdk.api.CardAPI;
import io.magicthegathering.javasdk.resource.Card;
import io.magicthegathering.javasdk.resource.ForeignData;

public class Helper {


    public static Card getCard(String aCardName) {
        Card card = new Card();
        try {
            card = CardAPI.getCard(aCardName);
        }  catch (Exception e) {
            System.out.println("Es ist ein Fehler aufgetreten: " +e);
        }
        return card;
    }

    public static Card getCardsByName (String aCardName) {
        Card card = new Card();
        try {
            List<String> filters = new ArrayList<String>();
               filters.add("name=" + aCardName);
            List<Card> possibleCards = CardAPI.getAllCards(filters);
            if(null != possibleCards){
                for(int i = 0; i < possibleCards.size(); i++){
                    if(aCardName.equals(possibleCards.get(i).getName())) {
                        card = possibleCards.get(i);
                        break;
                    }
                }
            }
        }  catch (Exception e) {
            System.out.println("Es ist ein Fehler aufgetreten: " +e);
        }
        return card;
    }

    //Maybe buggy
    public static Card getCardsByGermanName (String aCardName) {
        Card card = new Card();
        try {
            List<String> filters = new ArrayList<String>();
            ForeignData foreignNames = new ForeignData();
            foreignNames.setLanguage("german");
            foreignNames.setName(aCardName);

            filters.add("foreignNames" + foreignNames);

            List<Card> possibleCards = CardAPI.getAllCards(filters);
            if(null != possibleCards){
                for(int i = 0; i < possibleCards.size(); i++){
                    if(aCardName.equals(possibleCards.get(i).getName())) {
                        card = possibleCards.get(i);
                        break;
                    }
                }
            }
        }  catch (Exception e) {
            System.out.println("Es ist ein Fehler aufgetreten: " +e);
        }
        return card;
    }
}
