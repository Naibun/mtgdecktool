package Card;

public class CardConstants {

    // Cardtypes
    public static final String CREATURE = "creature";
    public static final String PLANESWALKER = "planeswalker";
    public static final String ENCHANTMENT = "enchantment";
    public static final String ARTIFACT = "artifact";
    public static final String INSTANT = "instant";
    public static final String SORCERY = "sorcery";
    public static final String BASIC_LAND = "basic land";

    // Colors
    public static final String WHITE = "W";
    public static final String BLUE = "U";
    public static final String BLACK = "B";
    public static final String RED = "R";
    public static final String GREEN = "G";
}
