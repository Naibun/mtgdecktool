package WriterReader;

import org.junit.Assert;
import org.junit.Test;

import Deck.Deck;

public class FileReaderTest {

    @Test
    public void testLoad() {

        // given
        FileReader reader = new FileReader();
        String fileName = "newDeck";
        Deck loadedDeck = null;

        // when
        loadedDeck = reader.load(fileName);

        // then

        Assert.assertTrue(1 == loadedDeck.getDeckList().size());
    }
}
