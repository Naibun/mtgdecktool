package WriterReader;

import org.junit.Test;

import java.util.List;

import Card.CardConstants;

import Deck.Deck;
import Deck.CommanderDeck;
import Deck.DeckConstants;
import Deck.DeckFactory;
import Deck.DeckList.DeckList;
import io.magicthegathering.javasdk.resource.Card;

public class FileWriterTest {

    @Test
    public void testSave() {
        // given
        DeckFactory factory = new DeckFactory();
        CommanderDeck testDeck = new CommanderDeck();

        List<Card> list = DeckList.getDeckListInstance(testDeck.getDeckSize());
        testDeck.setDeckList(list);

        testDeck.setDeckSize();

        Card commander = new Card();
        String[] colorIdentity = new String[1];
        colorIdentity[0] = CardConstants.WHITE;
        commander.setColorIdentity(colorIdentity);
        commander.setName("Avacyn, Angel of Hope");

        testDeck.setCommander(commander);

        Card card1 = new Card();
        card1.setColorIdentity(colorIdentity);
        card1.setName("Hunted Witness");

        testDeck.addCardToList(card1);

        // when

        FileWriter writer = new FileWriter();

        writer.save(testDeck);
        // then

    }
}
