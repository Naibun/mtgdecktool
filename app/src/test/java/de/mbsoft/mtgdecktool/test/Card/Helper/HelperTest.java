package de.mbsoft.mtgdecktool.test.Card.Helper;

import org.junit.Assert;
import org.junit.Test;

import Card.Helper.Helper;

import io.magicthegathering.javasdk.resource.Card;

public class HelperTest {
    @Test
    public void testGetCard () {
        String aCardNameEnglish = "Avacyn, Angel of Hope";
        String aCardNameGerman = "Avacyn, Engel der Hoffnung";

        Card resultCardGerman = null;

        Card resultCardEnglish = null;

        resultCardEnglish = Helper.getCardsByName(aCardNameEnglish);


        //Still buggy, it seems..
      //  resultCardGerman = Helper.getCardsByGermanName(aCardNameGerman);

        //Assert.assertNotNull(resultCardGerman);
        Assert.assertNotNull(resultCardEnglish);

    }
}
