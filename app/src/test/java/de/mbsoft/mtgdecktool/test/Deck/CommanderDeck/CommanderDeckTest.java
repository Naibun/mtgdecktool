package de.mbsoft.mtgdecktool.test.Deck.CommanderDeck;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import Card.CardConstants;

import Deck.Deck;
import Deck.DeckFactory;
import Deck.DeckConstants;
import Deck.DeckList.DeckList;
import io.magicthegathering.javasdk.resource.Card;


public class CommanderDeckTest {
    @Test
    public void testCreateDeck() {
        //given
        String format = DeckConstants.COMMANDER;
        int maxSize = 0;
        //when
        DeckFactory factory = new DeckFactory();
        Deck deck = factory.createNewDeck(format);

        List<Card> list = DeckList.getDeckListInstance(deck.getDeckSize());

        //then
        Assert.assertNotNull(list);

    }

    @Test
    public void testAddCardToList() {
        //given
        DeckFactory factory = new DeckFactory();
        Deck commanderDeck = factory.createNewDeck(DeckConstants.COMMANDER);

        List<Card> decklist = DeckList.getDeckListInstance((commanderDeck.getDeckSize()));
        String[] colorIdentity = new String[2];
        colorIdentity[0] = CardConstants.WHITE;
        colorIdentity[1] = CardConstants.RED;

        commanderDeck.setDeckSize();
        commanderDeck.setDeckList(decklist);

        commanderDeck.setColorIdentity(colorIdentity);

        Card testCard1 = new Card();
        Card testCard2 = new Card();

        testCard1.setName("Avacyn, Angel of Hope");
        String[] colorIdentityCard1 = new String[1];
        colorIdentityCard1[0] = CardConstants.WHITE;
        testCard1.setColorIdentity(colorIdentityCard1);

        testCard2.setName("Teysa Karlov");
        String[] colorIdentityCard2 = new String[2];
        colorIdentityCard2[0] = CardConstants.WHITE;
        colorIdentityCard2[1] = CardConstants.BLACK;
        testCard2.setColorIdentity(colorIdentityCard2);

        //when

        commanderDeck.addCardToList(testCard1);
        commanderDeck.addCardToList(testCard2);
        commanderDeck.addCardToList(testCard1);

        //then
        Assert.assertNotNull(commanderDeck.getDeckList());
        Assert.assertTrue(1 == commanderDeck.getActualDeckSize());
        Assert.assertTrue("Avacyn, Angel of Hope".equals(commanderDeck.getDeckList().get(0).getName()));
    }

    @Test
    public void testRemoveCardFromList() {
        // given
        DeckFactory factory = new DeckFactory();
        Deck commanderDeck = factory.createNewDeck(DeckConstants.COMMANDER);

        List<Card> decklist = DeckList.getDeckListInstance((commanderDeck.getDeckSize()));
        String[] colorIdentity = new String[2];
        colorIdentity[0] = CardConstants.WHITE;
        colorIdentity[1] = CardConstants.RED;

        commanderDeck.setDeckSize();
        commanderDeck.setDeckList(decklist);

        commanderDeck.setColorIdentity(colorIdentity);

        Card testCard1 = new Card();
        Card testCard2 = new Card();

        testCard1.setName("Avacyn, Angel of Hope");
        String[] colorIdentityCard1 = new String[1];
        colorIdentityCard1[0] = CardConstants.WHITE;
        testCard1.setColorIdentity(colorIdentityCard1);

        testCard2.setName("Aurelia the Warleader");
        String[] colorIdentityCard2 = new String[2];
        colorIdentityCard2[0] = CardConstants.WHITE;
        colorIdentityCard2[1] = CardConstants.RED;
        testCard2.setColorIdentity(colorIdentityCard2);

        commanderDeck.addCardToList(testCard1);
        commanderDeck.addCardToList(testCard2);

        // when
        commanderDeck.removeCardFromList("Avacyn, Angel of Hope");

        // then
        Assert.assertTrue(1 == commanderDeck.getActualDeckSize());

    }
}
