package de.mbsoft.mtgdecktool.test.Deck.DeckList;

import android.support.v4.widget.TextViewCompat;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import Deck.DeckList.DeckList;
import io.magicthegathering.javasdk.resource.Card;

public class DeckListTest {

    @Test
    public void testGetDeckListInstance() {
        //given
        int maxSize = 100;

        //when
        List<Card> deckList = DeckList.getDeckListInstance(maxSize);

        //then
        Assert.assertNotNull(deckList);
        Assert.assertTrue(0 == deckList.size());

    }

    @Test
    public  void testAddCard(){
        //given
        int maxSize = 100;
        String cardName = "Avacyn, Angel of Hope";
        Card aCard = new Card();
        aCard.setName(cardName);
        List<Card> deckList = DeckList.getDeckListInstance(maxSize);

        //when

        DeckList.addCardToDeckList(aCard);

        //then
        Assert.assertTrue(1 == deckList.size());

        Assert.assertTrue(cardName.equals(deckList.get(0).getName()));
    }

}

